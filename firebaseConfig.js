// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getDatabase } from "firebase/database";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCqbg6wfT5jEZH1YXnQaFkSWfx8f1_OQcE",
  authDomain: "time-ce874.firebaseapp.com",
  projectId: "time-ce874",
  storageBucket: "time-ce874.appspot.com",
  messagingSenderId: "243163420859",
  appId: "1:243163420859:web:53305f021b7a20364bb19e",
  databaseURL:
    "https://time-ce874-default-rtdb.europe-west1.firebasedatabase.app",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// Initialize Realtime Database and get a reference to the service
const database = getDatabase(app);
